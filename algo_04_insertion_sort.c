#include <stdio.h>

void insertion_sort(int * arr, int arr_size);

int main()
{
    int i;
    int arr[] = {10, 5, 6, 7, 8, 9};
    int arr_size = sizeof(arr) / sizeof(* arr);

    printf("Before sorting: ");
    for(i=0; i < arr_size; i++){
        printf("%d ", arr[i]);
    }
    
    insertion_sort(arr, arr_size);
    printf("\nInsertion sort: ");
    for(i=0; i < arr_size; i++){
        printf("%d ", arr[i]);
    }

    return 0;
}

/********************************************/

void insertion_sort(int * arr, int arr_size){
    int key;
    int i, j;

    for(i=1; i < arr_size; i++){
        key = arr[i];
        j = i-1;

        while(j>=0 && arr[j] > key){
            arr[j+1] = arr[j];
            --j;
        }
        arr[j+1] = key;
    }

    return;
}