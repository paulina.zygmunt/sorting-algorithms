#include <stdio.h>
//#define arr_size 6

void bubble_sort_1(int * arr, int arr_size);
void bubble_sort_2(int * arr, int arr_size);
void bubble_sort_3(int * arr, int arr_size);

int main()
{
//	const int arr_size=6;
	int arr[] = {10, 5, 6, 7, 8, 9};
	int arr_size = sizeof arr / sizeof *arr;
	int n;

	printf("unsorted array:\n");
	for(n=0; n<arr_size; n++){
		printf("%d ", arr[n]);
	}

	printf("\nbubble sort:");
	
	bubble_sort_1(arr, arr_size);
	printf("\n1) ");
	for(n=0; n < arr_size; n++){
		printf("%d ", arr[n]);
	}
	
	bubble_sort_2(arr, arr_size);
	printf("\n2) ");
	for(n=0; n < arr_size; n++){
		printf("%d ", arr[n]);
	}

	bubble_sort_3(arr, arr_size);
	printf("\n3) ");
	for(n=0; n < arr_size; n++){
		printf("%d ", arr[n]);
	}

	return 0;
}

/***********************************************/

void bubble_sort_1(int * arr, int arr_size)
{
	int outer, inner, temp;
	for(outer=0; outer < arr_size ; outer++){
		for(inner=0; inner < arr_size -1 ; inner++){
			if(arr[inner] > arr[outer]){ //sorts in ascending order
				temp = arr[outer];
				arr[outer] = arr[inner];
				arr[inner] = temp;
			}
		}
	}
	return;
}


void bubble_sort_2(int * arr, int arr_size)
{
	int outer, inner, temp;
	for(outer=0; outer < arr_size - 1 ; outer++){
		for(inner=outer; inner < arr_size ; inner++){
			if(arr[inner] < arr[outer]){ //sorts in ascending order
				temp = arr[outer];
				arr[outer] = arr[inner];
				arr[inner] = temp;
			}
		}
	}
	return;
}


void bubble_sort_3(int * arr, int arr_size)
{
	int outer, inner, temp;
	int didSwap; //changes to 1 if there was a swap
	for(outer=0; outer < arr_size - 1 ; outer++){
		didSwap = 0; //
		for(inner=outer; inner < arr_size ; inner++){
			if(arr[inner] < arr[outer]){ //sorts in ascending order
				temp = arr[outer];
				arr[outer] = arr[inner];
				arr[inner] = temp;
				didSwap = 1;
			}
			if(didSwap==0){
				break;
			}
		}
	}
	return;
}
